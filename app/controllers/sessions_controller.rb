class SessionsController < ApplicationController

  def new
  end

  def create
    @user = User.find_by(email: params[:session][:email].downcase)
    if @user && @user.authenticate(params[:session][:password])
      log_in(@user) #Logs in the @user.
      params[:session][:remember_me] == '1' ? remember(@user) : forget(@user)
      redirect_to(@user) #Redirects @user to the show page.
    else
      flash.now[:danger] = 'Invalid email/password combintaion'
      render('new')
    end
  end

  def destroy
    log_out if logged_in? #Logs out the user only if #logged_in? == true
    redirect_to(root_url)
  end

end
