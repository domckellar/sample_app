require 'test_helper'

class UsersLoginTest < ActionDispatch::IntegrationTest

  def setup
    @user = users(:dominic)
  end

  test "login with invalid information" do
    get login_path
    assert_template 'sessions/new'
    post login_path, session: { email: "", password: "" } #Post to the sessions path with an invalid params hash
    assert_template 'sessions/new' #Verify that the new sessions form get re-rendered.
    assert_not flash.empty? #Verify flash message appears.
    get root_path
    assert flash.empty? #Verify that the flash message doesn't appear on the new page.
  end

  test "login with valid information followed by logout" do
    # Login
    get login_path
    post login_path, session: { email: @user.email, password: 'password' }
    assert is_logged_in? #Checks if user is logged in.
    assert_redirected_to @user  #Check the redirect target
    follow_redirect! #Visit the targeted page (following the redirect)
    assert_template 'users/show'
    assert_select "a[href=?]", login_path, count: 0
    assert_select "a[href=?]", logout_path
    assert_select "a[href=?]", user_path(@user)
    # Logout
    delete logout_path
    assert_not is_logged_in?
    assert_redirected_to root_url
    #Simulate a user clicking logout in a second window.
    delete logout_path
    follow_redirect!
    # assert_template 'static_pages/home'
    assert_select "a[href=?]", login_path
    assert_select "a[href=?]", logout_path,      count: 0
    assert_select "a[href=?]", user_path(@user), count: 0
  end

  test "login with remembering" do
    log_in_as(@user, remember_me: '1')
    assert_equal cookies['remember_token'], assigns(:user).remember_token
    # assert_not_nil cookies['remember_token']
  end

  test "login without remembering" do
    log_in_as(@user, remember_me: '0')
    assert_nil cookies['remember_token']
  end

end
